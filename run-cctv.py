import os
import time
import cv2
import base64
from dotenv import load_dotenv
load_dotenv()

URLCCTV = os.getenv('RTSP_URL')

if URLCCTV == "":
    print("error config")
    while True:
        time.sleep(1)

time.sleep(0.1)

cctv = ""
namaImg = ""

def stream(sourceCam):
    global namaImg
    namaImg = "screenshots/capture.jpg"
    # print(sourceCam)
    # print(namaImg)
    cap = cv2.VideoCapture(sourceCam, cv2.CAP_FFMPEG)

    if(cap.isOpened()):
        ret, frame = cap.read()

        if ret == True:
            # print("Capturing cam")
            #cv2.moveWindow('Streaming', 0,0) #disable jika run di linux server
            cv2.imwrite(namaImg,frame)
            #cv2.imshow('Streaming', frame)  #disable jika run di linux server
        else:
            # print("release cam and start again")
            cap.release()
            cap = cv2.VideoCapture(sourceCam)
    else:
        print("error cam no open")
        namaImg = "default.png"

    cap.release()
    cv2.destroyAllWindows()

def run():
    global cctv
    try:
        with open(namaImg, "rb") as img_file:
            cctv = base64.b64encode(img_file.read())
        cctv = cctv.decode('utf-8')
        print(cctv)
    except Exception as E:
        print('error: {}'.format(str(E)))

def cmd():
    global cctv
    stream(URLCCTV)
    run()

if __name__=='__main__':
    # while True:
    cmd()
    # run()
