import os
import time
import random
import serial
import socket
import json
import cv2
from dotenv import load_dotenv
load_dotenv()

RfidMode = os.getenv('RFID_MODE', 'single')
TypeCon = os.getenv('TYPE(SERIAL_1-TCP_2)')
COMPort = os.getenv('COMPORT')
BAUDRATE = os.getenv('BAUDRATE')
IP_RFID_Reader = os.getenv('IPRFID')
PORTTcp = os.getenv('PORTTCP')
runpersec = os.getenv('RFID_RUN_X', 15)

RFID_Reader = IP_RFID_Reader                # ip rfid reader
port_RFID_Reader = PORTTcp                  # port rfid reader

EPC1 = "AA 02 10 00 02 01 01 71 AD"         # EPC antenna 1
EPC2 = "AA 02 10 00 02 02 01 7B AD"         # EPC antenna 2

cmdStop = "AA 02 FF 00 00 A4 0F"
cmdInfoDevice = "AA 02 09 00 05 01 0B B8 02 00 1D EB"

if TypeCon == "1":
    try:
        test_serial = serial.Serial(
                    port=COMPort,
                    baudrate=BAUDRATE,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=0.1)
        # print("RFID Serial Connection with Reader OK")
    except Exception as e:
        print('error: RFID Serial Connection')
        raise e

def crc(cmd):
    cmd = bytes.fromhex(cmd)
    return cmd


def send_cmd_serial(cmd):
    try:
        message = crc(cmd)
        test_serial.write(message)
        data = test_serial.read(2560)
        response_hex = data.hex().upper()
        hex_list = [response_hex[i:i + 2] for i in range(0, len(response_hex), 2)]
        hex_space = ' '.join(hex_list)
        return data
    except Exception as e:
        print('error connection')
        raise e


def send_cmd(cmd):
    try:
        message = crc(cmd)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(0.2)
        s.connect((RFID_Reader, int(port_RFID_Reader)))
        s.sendall(message)
        data = s.recv(2560)
        response_hex = data.hex().upper()
        hex_list = [response_hex[i:i + 2] for i in range(0, len(response_hex), 2)]
        hex_space = ' '.join(hex_list)
        s.close()
        return data
    except Exception as e:
        print('error connection')
        raise e

# try:
#     if TypeCon == "1":  HEX_data = send_cmd_serial(cmdInfoDevice)
#     else:               HEX_data = send_cmd(cmdInfoDevice)
# except Exception as e:
#     print(e)
#     raise e


def cmd(antena, stop):
    try:
        if (stop == 0):
            if TypeCon == "1":  HEX_data = send_cmd_serial(cmdStop)
            else:               HEX_data = send_cmd(cmdStop)
        else:
            if TypeCon == "1":                                      # serial
                if antena == 1: HEX_data = send_cmd_serial(EPC1)
                else:           HEX_data = send_cmd_serial(EPC2)
            else:                                                   # tcp
                if antena == 1: HEX_data = send_cmd(EPC1)
                else:           HEX_data = send_cmd(EPC2)
            header_check = HEX_data.hex().upper()
            return parsingdata(header_check)
    except Exception as e:
        print('error RFID connection')
        raise e

def parsingdata(header_check):
    ####### ITERATION BEGIN #######
    # RFID NEW
    parseLoop = False
    if header_check.find('AA12000015000C') > 0:
        fr = header_check.find('AA12000015000C') + 14
        to = header_check.find('34000101')
        if to > fr:
            dataTag = header_check[fr:to]
            print("EPC: " + dataTag)
            return dataTag
    elif header_check.find('AA12000013000C') > 0:
        fr = header_check.find('AA12000013000C') + 14
        to = header_check.find('34000101')
        if to > fr:
            dataTag = header_check[fr:to]
            print("EPC: " + dataTag)
            return dataTag
    elif header_check.find('AA1200001') > 0:
        fr = header_check.find('AA1200001') + 14
        to = header_check.find('34000101')
        if to > fr:
            dataTag = header_check[fr:to]
            print("EPC: " + dataTag)
            return dataTag
    else:
        parseLoop = True
        # RFID OLD
        if header_check[0:69] == "6361743A2063616E2774206F70656E20272F776966692F776966695F6163636573735F706F696E74273A204E6F20737563682066696C65206F72206469726563746F72790A":
            header_check = header_check[69:]
        dataAfterRemoveHead = header_check
        if header_check[0:26] == "AA100000060404021000024F9D":      # header check dengan data   AA100000060404021000024F9DAA1200001C000CE280699500004001BE23992F3000010141074599527C00086DDC9C37
            if header_check == "AA100000060404021000024F9D":        # header check tanpa data
                return ''
                # print("no tag")
            else:
                dataAfterRemoveHead = header_check[26:]
                # print(dataAfterRemoveHead)
        if header_check[0:16] == "AA021000010046F6":                # header check dengan data   AA021000010046F6AA1200001C000CE2000020341301590610849B30000101510745995945000EFD898DE4AA1200001C000CE20000203413015306107BDC30000101690745995945000EFE7BCB31AA1200001C000CE32D77FCA12015060201271F30000101580745995945000F24BD10C1AA1200001C000CE2000020340F02812050F4A430000101620745995945000F25A5ECB3AA1200001C000CE32D77FCA12015060201154530000101530745995946000009792BF8AA120000180008ABCDEF01234567892000010173074599594600000A63A6B2AA1200001C000CE32D77FCA12015060201264F300001015507459959460000306BDD7F
            dataAfterRemoveHead = header_check[16:]
            # print(dataAfterRemoveHead)
        # ITERATION FOR UNCOMPLETE DATA
        while parseLoop:
            parseLoop = False
            # sering muncul msg ini-> "cat: can't open '/wifi/wifi_access_point': No such file or directory{0A}"
            if dataAfterRemoveHead[0:69] == "6361743A2063616E2774206F70656E20272F776966692F776966695F6163636573735F706F696E74273A204E6F20737563682066696C65206F72206469726563746F72790A":
                dataAfterRemoveHead = dataAfterRemoveHead[69:]

            if dataAfterRemoveHead[0:12] == "AA1200001300":   #HF100             #header tanpa (no tag) AA021000010046F6AA12000013000CE20000203413015306107BDC300001015812F3
                dataAfterRemoveHead = dataAfterRemoveHead[12:]
                lengthData = bytes.fromhex(dataAfterRemoveHead[0:2])
                lengthData = int.from_bytes(lengthData, byteorder='big', signed=False)
                lengthData = lengthData*2   #panjang karakter HEX
                dataAfterRemoveHead = dataAfterRemoveHead[2:]   #remove length data
                dataTag = dataAfterRemoveHead[0:lengthData]
                dataAfterRemoveHead = dataAfterRemoveHead[lengthData+14:]   #14 penutup data EPC
                print("EPC: " + dataTag)
                parseLoop = True
                return dataTag
                # print("good tag 12 detected (6Word - 24Hex) --- Antena " + str(antena))

            if dataAfterRemoveHead[0:12] == "AA1200000F00":                #header tanpa (no tag) AA1200000F00081234567890ABCDEF200001017132E9AA1200000F00081234567890ABCDEF2000010173B2E6
                dataAfterRemoveHead = dataAfterRemoveHead[12:]
                lengthData = bytes.fromhex(dataAfterRemoveHead[0:2])
                lengthData = int.from_bytes(lengthData, byteorder='big', signed=False)
                lengthData = lengthData*2   #panjang karakter HEX
                dataAfterRemoveHead = dataAfterRemoveHead[2:]   #remove length data
                dataTag = dataAfterRemoveHead[0:lengthData]
                dataAfterRemoveHead = dataAfterRemoveHead[lengthData+14:]   #14 penutup data EPC
                print("EPC: " + dataTag)
                parseLoop = True
                return dataTag
                # print("good tag 8 detected (4Word - 16Hex) --- Antena " + str(antena))


if __name__=='__main__':
    if True:
        cmd(1, 0)
        for x in range(int(runpersec)):
            cmd(1, 1)
        cmd(1, 0)
