import os
import time
import random
import serial
import socket
import json
import cv2
import base64
from paho.mqtt import client as mqtt
from dotenv import load_dotenv
load_dotenv()

RfidMode = os.getenv('RFID_MODE', 'single')
TypeCon = os.getenv('TYPE(SERIAL_1-TCP_2)')
COMPort = os.getenv('COMPORT')
BAUDRATE = os.getenv('BAUDRATE')
IP_RFID_Reader = os.getenv('IPRFID')
PORTTcp = os.getenv('PORTTCP')
TopicMQTT1 = os.getenv('TOPICMQTTANTENNA1')
TopicMQTT2 = os.getenv('TOPICMQTTANTENNA2')
BrokerMQTT = os.getenv('BROKERMQTT', False)

print(RfidMode)
print(TypeCon)
print(COMPort)
print(BAUDRATE)
print(IP_RFID_Reader)
print(PORTTcp)
print(TopicMQTT1)
print(TopicMQTT2)
print(BrokerMQTT)

if IP_RFID_Reader == "" or PORTTcp == "" or TypeCon == "" or COMPort == "" or BAUDRATE == "" or TypeCon == "" or BrokerMQTT == "":
    print("error config")
    while True:
        time.sleep(1)

if TypeCon == "1":
    try:
        test_serial = serial.Serial(
                    port=COMPort,
                    baudrate=BAUDRATE,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=0.1)
        print("Serial Connection with Reader OK")
    except Exception as e:
        print("Serial Connection with Reader ERROR")
        print(e)
        while True:
            time.sleep(1)
elif TypeCon == "2":
    print("TCP Connection")
else:
    print("Error Type Connection")
    while True:
        time.sleep(1)

RFID_Reader = IP_RFID_Reader                # ip rfid reader
port_RFID_Reader = PORTTcp                  # port rfid reader


EPC1 = "AA 02 10 00 02 01 01 71 AD"         # EPC antenna 1
EPC2 = "AA 02 10 00 02 02 01 7B AD"         # EPC antenna 2


cmdStop = "AA 02 FF 00 00 A4 0F"
cmdInfoDevice = "AA 02 09 00 05 01 0B B8 02 00 1D EB"
#cmdInfoDevice = "AA 02 00 00 00 A8 03"

protocolMQTT = True
if BrokerMQTT == False: protocolMQTT = False

broker = BrokerMQTT
port = 1883
client_id = f'python-mqtt-{random.randint(0, 1000)}'

client = mqtt.Client(client_id)


def crc(cmd):
    cmd = bytes.fromhex(cmd)
    return cmd


def send_cmd(cmd):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((RFID_Reader, int(port_RFID_Reader)))
    message = crc(cmd)
    s.sendall(message)
    data = s.recv(2560)
    response_hex = data.hex().upper()
    hex_list = [response_hex[i:i + 2] for i in range(0, len(response_hex), 2)]
    hex_space = ' '.join(hex_list)
    #print(hex_space)
    s.close()
    return data


def send_cmd_serial(cmd):
    message = crc(cmd)
    test_serial.write(message)
    data = test_serial.read(2560)
    response_hex = data.hex().upper()
    hex_list = [response_hex[i:i + 2] for i in range(0, len(response_hex), 2)]
    hex_space = ' '.join(hex_list)
    #print(hex_space)
    return data

print()
print()

try:
    if TypeCon == "1":
        HEX_data = send_cmd_serial(cmdStop)
    else:
        HEX_data = send_cmd(cmdStop)
    print('cmd-stop')
except Exception as e:
    print(e)