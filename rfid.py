import os
import time
import random
import serial
import socket
import json
import cv2
import base64
from paho.mqtt import client as mqtt
from dotenv import load_dotenv
load_dotenv()

RfidMode = os.getenv('RFID_MODE', 'single')
TypeCon = os.getenv('TYPE(SERIAL_1-TCP_2)')
COMPort = os.getenv('COMPORT')
BAUDRATE = os.getenv('BAUDRATE')
IP_RFID_Reader = os.getenv('IPRFID')
PORTTcp = os.getenv('PORTTCP')
TopicMQTT1 = os.getenv('TOPICMQTTANTENNA1')
TopicMQTT2 = os.getenv('TOPICMQTTANTENNA2')
BrokerMQTT = os.getenv('BROKERMQTT', False)

print('RfidMode: ', RfidMode)
print('TypeCon(Serial / TCP): ', TypeCon)
print('COMPort: ', COMPort)
print('BAUDRATE: ', BAUDRATE)
print('IP_RFID_Reader: ', IP_RFID_Reader)
print('PORTTcp: ', PORTTcp)
print('BrokerMQTT', BrokerMQTT)
print('TopicMQTT1', TopicMQTT1)
print('TopicMQTT2', TopicMQTT2)

if IP_RFID_Reader == "" or PORTTcp == "" or TypeCon == "" or COMPort == "" or BAUDRATE == "" or TypeCon == "" or BrokerMQTT == "":
    print("error config")
    while True:
        time.sleep(5)

if TypeCon == "1":
    try:
        test_serial = serial.Serial(
                    port=COMPort,
                    baudrate=BAUDRATE,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=0.1)
        print("Serial Connection with Reader OK")
    except Exception as e:
        print("Serial Connection with Reader ERROR")
        print(e)
        while True:
            time.sleep(1)
elif TypeCon == "2":
    print("TCP Connection")
else:
    print("Error Type Connection")
    while True:
        time.sleep(1)

RFID_Reader = IP_RFID_Reader                # ip rfid reader
port_RFID_Reader = PORTTcp                  # port rfid reader


EPC1 = "AA 02 10 00 02 01 01 71 AD"         # EPC antenna 1
EPC2 = "AA 02 10 00 02 02 01 7B AD"         # EPC antenna 2


cmdStop = "AA 02 FF 00 00 A4 0F"
cmdInfoDevice = "AA 02 09 00 05 01 0B B8 02 00 1D EB"
#cmdInfoDevice = "AA 02 00 00 00 A8 03"

protocolMQTT = True
if BrokerMQTT == False: protocolMQTT = False

broker = BrokerMQTT
port = 1883
client_id = f'python-mqtt-{random.randint(0, 1000)}'

client = mqtt.Client(client_id)


def crc(cmd):
    cmd = bytes.fromhex(cmd)
    # print(cmd)
    return cmd


def send_cmd(cmd):
    message = crc(cmd)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(0.5)
    s.connect((RFID_Reader, int(port_RFID_Reader)))
    s.sendall(message)
    data = s.recv(2560)
    response_hex = data.hex().upper()
    hex_list = [response_hex[i:i + 2] for i in range(0, len(response_hex), 2)]
    hex_space = ' '.join(hex_list)
    #print(hex_space)
    s.close()
    return data


def send_cmd_serial(cmd):
    message = crc(cmd)
    # print('message', message)
    test_serial.write(message)
    data = test_serial.read(2560)
    response_hex = data.hex().upper()
    hex_list = [response_hex[i:i + 2] for i in range(0, len(response_hex), 2)]
    hex_space = ' '.join(hex_list)
    # print('hex-space', hex_space)
    return data

# print()
# print()

try:
    if TypeCon == "1":
        HEX_data = send_cmd_serial(cmdStop)
    else:
        HEX_data = send_cmd(cmdStop)
    print('cmd-stop')
    # print(HEX_data.hex().upper())
except Exception as e:
    print(e)

try:
    if TypeCon == "1":
        HEX_data = send_cmd_serial(cmdInfoDevice)
    else:
        HEX_data = send_cmd(cmdInfoDevice)
    # print('cmd-info-device')
    # print(HEX_data.hex().upper())
except Exception as e:
    print(e)

data_RFID = []
countData = 0

time.sleep(0.1)

rfid = ""
cctv = ""
namaImg = ""
URLCCTV = ""
apiurl = ""

changeAntenna = 1

def cmd(antena, stop):
    global countData
    global data_RFID
    global rfid
    global changeAntenna
    global topic

    countData = 0
    data_RFID = []
    try:
        if TypeCon == "1":                                      # serial
            if stop == 1:
                HEX_data = send_cmd_serial(cmdStop)
                # print("stoooooooooooooooooooop")
            # print("-- antena {} --".format(antena))
            if antena == 1:
                #print("antenna 1")
                topic = TopicMQTT1
                HEX_data = send_cmd_serial(EPC1)
            else:
                #print("antenna 2")
                topic = TopicMQTT2
                HEX_data = send_cmd_serial(EPC2)
        else:                                                   #tcp
            if stop == 1:
                HEX_data = send_cmd(cmdStop)
                # print("stoooooooooooooooooooop")
            # print("-- antena {} --".format(antena))
            if antena == 1:
                topic = TopicMQTT1
                HEX_data = send_cmd(EPC1)
            else:
                # print("antenna 2")
                topic = TopicMQTT2
                HEX_data = send_cmd(EPC2)
        # print(HEX_data.hex().upper())
        header_check = HEX_data.hex().upper()
        rfid = parsingdata(header_check)
        if protocolMQTT and rfid:
            try:
                client.connect(broker, port)
                MQTT_MESSAGE = rfid
                client.loop_start()
                result = client.publish(topic, MQTT_MESSAGE)
                status = result[0]
                if status == 0:
                    print(f"Send data to topic `{topic}` success")
                else:
                    print(f"Failed to send message to topic {topic}")
                client.loop_stop()
            except Exception as e:
                print("error mqtt")
                print(e)
    except Exception as e:
            print(e)


def parsingdata(header_check):
    ####### ITERATION BEGIN #######
    # RFID NEW
    parseLoop = False
    if header_check.find('AA12000015000C') > 0:
        fr = header_check.find('AA12000015000C') + 14
        to = header_check.find('34000101')
        if to > fr:
            dataTag = header_check[fr:to]
            print("EPC: " + dataTag)
            return dataTag
    elif header_check.find('AA12000013000C') > 0:
        fr = header_check.find('AA12000013000C') + 14
        to = header_check.find('34000101')
        if to > fr:
            dataTag = header_check[fr:to]
            print("EPC: " + dataTag)
            return dataTag
    elif header_check.find('AA1200001') > 0:
        fr = header_check.find('AA1200001') + 14
        to = header_check.find('34000101')
        if to > fr:
            dataTag = header_check[fr:to]
            print("EPC: " + dataTag)
            return dataTag
    else:
        parseLoop = True
        # RFID OLD
        if header_check[0:69] == "6361743A2063616E2774206F70656E20272F776966692F776966695F6163636573735F706F696E74273A204E6F20737563682066696C65206F72206469726563746F72790A":
            header_check = header_check[69:]
        dataAfterRemoveHead = header_check
        if header_check[0:26] == "AA100000060404021000024F9D":      # header check dengan data   AA100000060404021000024F9DAA1200001C000CE280699500004001BE23992F3000010141074599527C00086DDC9C37
            if header_check == "AA100000060404021000024F9D":        # header check tanpa data
                return None
                # print("no tag")
            else:
                dataAfterRemoveHead = header_check[26:]
                # print(dataAfterRemoveHead)
        if header_check[0:16] == "AA021000010046F6":                # header check dengan data   AA021000010046F6AA1200001C000CE2000020341301590610849B30000101510745995945000EFD898DE4AA1200001C000CE20000203413015306107BDC30000101690745995945000EFE7BCB31AA1200001C000CE32D77FCA12015060201271F30000101580745995945000F24BD10C1AA1200001C000CE2000020340F02812050F4A430000101620745995945000F25A5ECB3AA1200001C000CE32D77FCA12015060201154530000101530745995946000009792BF8AA120000180008ABCDEF01234567892000010173074599594600000A63A6B2AA1200001C000CE32D77FCA12015060201264F300001015507459959460000306BDD7F
            dataAfterRemoveHead = header_check[16:]
            # print(dataAfterRemoveHead)
        # ITERATION FOR UNCOMPLETE DATA
        while parseLoop:
            parseLoop = False
            # sering muncul msg ini-> "cat: can't open '/wifi/wifi_access_point': No such file or directory{0A}"
            if dataAfterRemoveHead[0:69] == "6361743A2063616E2774206F70656E20272F776966692F776966695F6163636573735F706F696E74273A204E6F20737563682066696C65206F72206469726563746F72790A":
                dataAfterRemoveHead = dataAfterRemoveHead[69:]

            if dataAfterRemoveHead[0:12] == "AA1200001300":   #HF100             #header tanpa (no tag) AA021000010046F6AA12000013000CE20000203413015306107BDC300001015812F3
                dataAfterRemoveHead = dataAfterRemoveHead[12:]
                lengthData = bytes.fromhex(dataAfterRemoveHead[0:2])
                lengthData = int.from_bytes(lengthData, byteorder='big', signed=False)
                lengthData = lengthData*2   #panjang karakter HEX
                dataAfterRemoveHead = dataAfterRemoveHead[2:]   #remove length data
                dataTag = dataAfterRemoveHead[0:lengthData]
                dataAfterRemoveHead = dataAfterRemoveHead[lengthData+14:]   #14 penutup data EPC
                print("EPC: " + dataTag)
                parseLoop = True
                return dataTag
                # print("good tag 12 detected (6Word - 24Hex) --- Antena " + str(antena))

            if dataAfterRemoveHead[0:12] == "AA1200000F00":                #header tanpa (no tag) AA1200000F00081234567890ABCDEF200001017132E9AA1200000F00081234567890ABCDEF2000010173B2E6
                dataAfterRemoveHead = dataAfterRemoveHead[12:]
                lengthData = bytes.fromhex(dataAfterRemoveHead[0:2])
                lengthData = int.from_bytes(lengthData, byteorder='big', signed=False)
                lengthData = lengthData*2   #panjang karakter HEX
                dataAfterRemoveHead = dataAfterRemoveHead[2:]   #remove length data
                dataTag = dataAfterRemoveHead[0:lengthData]
                dataAfterRemoveHead = dataAfterRemoveHead[lengthData+14:]   #14 penutup data EPC
                print("EPC: " + dataTag)
                parseLoop = True
                return dataTag
                # print("good tag 8 detected (4Word - 16Hex) --- Antena " + str(antena))


ant = 1
loop = 0

if __name__=='__main__':
    # cmd(ant, 0)
    while True:
        if RfidMode == "single":
            loop += 1
            if loop == 19:
                cmd(ant, 1)
                loop = 0
            else:
                cmd(ant, 0)
        else:
            loop += 1
            if loop == 4:
                if ant == 1: ant = 2
                else: ant = 1
                cmd(ant, 1)
                loop = 0
            else:
                cmd(ant, 0)
    else:
        cmd(1, 1)



# pip3 install serial && pip3 install opencv-python && pip3 install paho-mqtt && pip3 install python-dotenv